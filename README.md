# Attacking and Defending a Web Service (Phase 1)#

### Usage:
For running the program, you will need the Gradle build tool and than follow the following steps:

1. run gradle installDist to set up everything; and
2. run ./build/install/p2.1-starter/bin/p2.1-starter

The service is accessible on port 9090


### What is this program.
* A web service that can handle a high number of requests at speed.
* This program can handle many concurrent requests with an aim to simulate a web service under a high load.
* It will make a total of _numRequests_ requests, but the number of concurrent requests at a moment is at most _maxConcurrent_
* The program will attempt to make as many requests as the client send with a benchmark of being able to handle 3000 or more requests/sec.